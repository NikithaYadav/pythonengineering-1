"""
Summary:

    String and common string methods: (Immutable)
        - replace(<src>, <target>) method - Replaces all occurrences
        - capitalize()
        - title()
        - upper()
        - lower()
        - str.join(list)   - converts a list to a string based on the given separator

    List and common list methods: (Mutable)
        - append(item)
        - extend(list)
        - insert(index, item)
        - remove(item)  - removes first occurrence
        - sort()

    Some utility functions
        - The range function
            - range(end)
            - range(start, end)
            - range(start, end, step)
            - Python3: type cast: list(range(n))

        - len()
        - del keyword
"""
# String and common string methods:

# - str.replace(<src>, <target>) method - Replaces all occurrences
welcome = 'Welcome to python language'
print(f'mem loc of welcome: {id(welcome)}')
print(welcome)
# print(Welcome)                                   # NameError: name 'Welcome' is not defined

# lang to programming
new_welcome = welcome.replace('language', 'programming')
print(f'mem loc of new_welcome: {id(new_welcome)}')
print(new_welcome)

new_welcome = welcome.replace('o', 'O')
print(f'mem loc of new_welcome: {id(new_welcome)}')
print(new_welcome)

# Lists are mutable
l = [1, 2, 'hello']
print(l)
print(f'mem loc of l: {id(l)}')

l.append('new message')
print(l)
print(f'mem loc of l: {id(l)}')

# - capitalize()
# print(f'capitalized: {welcome.capitalize()}')
print('capitalized: ' + welcome.capitalize())
# print('capitalized:', welcome.capitalize(), sep='****')


# - title()
print(f'title: {welcome.title()}')

# - upper()
print(f'upper: {welcome.upper()}')
print(f'is_lower: {welcome.islower()}')

# - lower()
welcome = welcome.lower()
print(f'lower: {welcome}')
print(f'is_lower: {welcome.islower()}')

# - str.join(list or tuple)   - converts a list to a string based on the given separator
input_list = ['swad', 'govind', 'chiranjivi', 'sairam']  # list of all string
output = 'swadgovindchiranjisairam'  # csv format

csv_string = ';'.join(input_list)  # TypeError: sequence item 0: expected str instance, float found
print(csv_string)

# Solve integer problem
# input_list = [1.04, 10, 'chiranjivi', 'sairam']        # list of all string
# string_list = [str(item) for item in input_list]       # list comprehension
# print(f'string list: {string_list}')
# csv_string = ';'.join(string_list)

# ---- List ----
# Mutable
# Lists are ordered
# List elements can be accessed by index; Example: print(names[10])
# Allows duplicates

# Common list methods:
#    - append(single item)
#    - extend(list) - l.extend(  [i, j, k]  )
#    - insert(index, item)
#    - remove(item)  - removes first occurrence

# append(element): Adding item to the end of the list -
companies = ['tcs', 'cts', 'hcl']
print(companies)

print(companies[0])  # first
print(companies[1])  # second
print(companies[-1])  # last

# how to add an item to a list (at the end)
companies.append('wipro')  # changes in place (mutable)
print(companies)

# extend(list or tuple or set) - at the end
new_companies = ['viasat', 'infy', 'syntel']  # list
# new_companies = ('viasat', 'infy', 'syntel')  # tuple
# new_companies = {'viasat', 'infy', 'syntel'}    # set
# new_companies = 'company'   # adds each item as a list element

companies.extend(new_companies)
print(companies)

# Adding item to the first
companies.insert(0, 'amazon')
# companies.insert(0, ['amazon', 'ibm'])
# Result: [['amazon', 'ibm'], 'tcs', 'cts', 'hcl', 'wipro', 'viasat', 'infy', 'syntel']
print(companies)

# Remove element using name. remove() first occurrence
companies.extend(['wipro', 'wipro'])
companies.remove('wipro')
print(companies)

# Sort ascending and descending elements within a list
companies.sort()  # in place sorting (meaning list is sorted within itself)
print(companies)

companies.sort(reverse=True)
print(companies)

students = [
    'chiru dhuva',
    'swadhi chand',
    'govind kaluva',
]
students.sort()
print(students)

# Find the index of an element in a list (first index)
wipro_index = companies.index('wipro')
print(wipro_index)

amazon_index = companies.index('amazon')
print(amazon_index)

print(companies[-1])
print(companies[-2])

# Some utility functions
#     - The range function (only for ints)
#         - range(end)     [end is exclusive]
ten_nos = range(10)
print(list(ten_nos))

#         - range(start, end)   # start is inclusive
ten_to_twenty = range(10, 20)  # [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
ten_to_twenty = list(ten_to_twenty)

print(ten_to_twenty)
print(f'size: {len(ten_to_twenty)}')

#     - range(start, end, step)
#     - range(start, end)   # start is inclusive
ten_to_twenty = range(100, 2000, 200)
for i in ten_to_twenty:
    print(i)

ten_to_twenty = list(ten_to_twenty)  # [100, 300, 500, 700, 900, 1100, 1300, 1500, 1700, 1900]
print(ten_to_twenty)
print(f'size: {len(ten_to_twenty)}')

# - Python3: type cast: list(range(n))

# Write a program, which prints square of all numbers divisible by 10
# from 0 to 100 (inclusive)
for item in range(10, 101, 10):  # 10, 20, 30 ..
    print(item ** 2)

"""
Interview questions:
    What is the use of string replace function?
    Using the string replace function can we replace characters?
    What is the difference between string append and extend method?
    How to insert at the beginning of a list?
    What is the use of range function? Give examples?
    In python 3how do we convert range to a list?
    How to convert a list to a string?
"""
